# disable implicit rules
.SUFFIXES:

NAME := my_stuff

INCDIR := include
SRCDIR := src
BLDDIR_ROOT := build
TSTDIR := tests

CC := clang
CFLAGS := -Wextra -Wall -pedantic -std=c11

ifeq ($(RELEASE), yes)
	CFLAGS += -O3
	BLDDIR := $(BLDDIR_ROOT)/release
	LOG_LEVEL ?= 1
else
	CFLAGS += -g3 -fstack-protector-all -Wshadow -Wunreachable-code \
		  -Wstack-protector -Werror -pedantic-errors -O0 -W -Wundef \
		  -Wfatal-errors -Wstrict-prototypes -Wmissing-prototypes \
		  -Wwrite-strings -Wunknown-pragmas -Wstrict-aliasing \
		  -Wold-style-definition -Wmissing-field-initializers \
		  -Wfloat-equal -Wpointer-arith -Wnested-externs \
		  -Wstrict-overflow=5 -Wswitch-default -Wswitch-enum \
		  -Wbad-function-cast -Wredundant-decls \
		  -fno-omit-frame-pointer -Winline -fstrict-aliasing
	BLDDIR := $(BLDDIR_ROOT)/debug
	LOG_LEVEL ?= 3
endif

CFLAGS += -DLOG_LEVEL=$(LOG_LEVEL)

TARGET := $(BLDDIR)/lib$(NAME).so
SRCS := $(shell find $(SRCDIR) -name *.c)
HEADERS := $(shell find $(INCDIR) -name *.h)
OBJS := $(SRCS:$(SRCDIR)/%.c=$(BLDDIR)/%.o)

all: srcblddirs $(TARGET)

$(TARGET): CFLAGS += -fPIC
$(TARGET): $(OBJS)
	@echo "Building $(NAME)"
	@$(CC) -shared $(CFLAGS) $^ -o $@

$(BLDDIR)/%.o: $(SRCDIR)/%.c $(INCDIR)/$(NAME)/%.h
	@echo "Compiling $*"
	@$(CC) -c $(CFLAGS) $< -I$(INCDIR) -o $@

SRCDIRS := $(shell find $(SRCDIR) -type d)
SRCBLDDIRS := $(SRCDIRS:$(SRCDIR)%=$(BLDDIR)%)
srcblddirs: 
	mkdir -p $(SRCBLDDIRS)

TSTBLDDIR := $(BLDDIR)/tests
TESTS := $(shell find $(TSTDIR) -name *.c)
TESTS := $(TESTS:$(TSTDIR)/%.c=$(TSTBLDDIR)/%)

.PHONY: test
test: tstblddirs $(TARGET) $(TESTS)
	@echo "[33m--------------- running tests ---------------[0m"
	@LD_LIBRARY_PATH=$$PWD/$(BLDDIR); \
	 export LD_LIBRARY_PATH; \
	 for t in $(TESTS); do \
	   if "./$$t"; then \
	     echo "[32m$$t: ✓[0m"; \
	   else \
	     echo "[31m$$t: ✕[0m"; \
	   fi \
	 done
	@echo "[33m---------------------------------------------[0m"

LFLAGS := -L$(BLDDIR)/ -l$(NAME)

$(TSTBLDDIR)/%: $(TSTDIR)/%.c
	@echo "Building test $*"
	@$(CC) $(CFLAGS) $< -I$(INCDIR) $(LFLAGS) -o $@

TSTDIRS := $(shell find $(TSTDIR) -type d)
TSTBLDDIRS := $(TSTDIRS:$(TSTDIR)%=$(TSTBLDDIR)%)
tstblddirs: srcblddirs
	mkdir -p $(TSTBLDDIRS)

.PHONY: clean
clean:
	rm -rf $(BLDDIR_ROOT)
