#include "my_stuff/core.h"
#include "my_stuff/result.h"

#include <string.h>

typedef const char* str;

def_result(int32_t, str)

int main() {
    Result(int32_t, str) o = Ok(5);
    Result(int32_t, str) e = Err("damn");

    assert(o.is_ok);
    assert(o.value == 5);
    assert(!e.is_ok);
    assert(strcmp(e.error, "damn") == 0);
}
