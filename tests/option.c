#include "my_stuff/core.h"

def_option(int32_t)

int main() {
    Option(int32_t) n = None;
    Option(int32_t) s = Some(5);

    assert(!n.is_some);
    assert(s.is_some);
    assert(s.value == 5);
}
