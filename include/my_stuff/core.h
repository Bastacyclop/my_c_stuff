#ifndef MY_CORE_H
#define MY_CORE_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <stdio.h>

#include "my_stuff/option.h"
#include "my_stuff/result.h"
#include "my_stuff/util.h"

def_option(size_t)

#define STRINGIFY(X) #X
#define STRINGIFY_EXPANDED(X) STRINGIFY(X)
#define LOC_INFO __FILE__":"STRINGIFY_EXPANDED(__LINE__)

void crash(void);

/// Is `n` a power of two ?
bool is_power_of_two(size_t n);

/// Returns the power of two following `n`.
size_t next_power_of_two(size_t n);

/// Returns the power of two following `n`, or none if there is an overflow.
Option(size_t) checked_next_power_of_two(size_t n);

#define def_max_min_for(T)      \
    T T##_max(T a, T b);        \
    T T##_min(T a, T b);

def_max_min_for(size_t)
def_max_min_for(uint8_t)
def_max_min_for(uint16_t)
def_max_min_for(uint32_t)
def_max_min_for(uint64_t)
def_max_min_for(int8_t)
def_max_min_for(int16_t)
def_max_min_for(int32_t)
def_max_min_for(int64_t)
def_max_min_for(float)
def_max_min_for(double)

#endif // MY_CORE_H
