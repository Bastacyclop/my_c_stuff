#ifndef MY_LOG_H
#define MY_LOG_H

#include "my_stuff/util.h"

FILE* get_log_file(void);
void set_log_file(FILE* f);

#define log(msg) fputs(msg"\n", get_log_file())
#define log_format(msg, ...) fprintf(get_log_file(), msg"\n", __VA_ARGS__)


#define INFO_STYLE(s) TERM_BLUE("info: "s)" ("LOC_INFO")"
#if LOG_LEVEL > 2
    #define info(s) log(INFO_STYLE(s))
    #define info_format(s, ...) log_format(INFO_STYLE(s), __VA_ARGS__)
#else
    #define info(s)
    #define info_format(s, ...)
#endif


#define WARNING_STYLE(s) TERM_YELLOW("warning: "s)" ("LOC_INFO")"
#if LOG_LEVEL > 1
    #define warning(s) log(WARNING_STYLE(s))
    #define waring_format(s, ...) log_format(WARNING_STYLE(s), __VA_ARGS__)
#else
    #define warning(s)
    #define warning_format(s, ...)
#endif


#define ERROR_STYLE(s) TERM_MAGENTA("error: "s)" ("LOC_INFO")"
#if LOG_LEVEL > 0
    #define error(s) log(ERROR_STYLE(s))
    #define error_format(s, ...) log_format(ERROR_STYLE(s), __VA_ARGS__)
#else
    #define error(s)
    #define error_format(s, ...)
#endif


static void (*fatal_handler)(void) = crash;

#define FATAL_STYLE(s) TERM_RED("fatal: "s)" ("LOC_INFO")"

#define fatal(s) { \
    log(FATAL_STYLE(s)); \
    fatal_handler(); \
}

#define fatal_format(s, ...) { \
    log_format(FATAL_STYLE(s), __VA_ARGS__); \
    fatal_handler(); \
}

#endif // MY_LOG_H
