#ifndef MY_MEM_H
#define MY_MEM_H

#include "my_stuff/core.h"

void* mem_alloc(size_t n);

void mem_free(void* mem);

void* mem_alloc_zeroed(size_t n);

void* mem_alloc_aligned(size_t n, size_t align);

void* mem_realloc(void* mem, size_t n);

void mem_set(void* mem, uint8_t byte, size_t n);

void mem_copy(const void* src, void* dst, size_t n);

/// Swaps the given values using a temporary stack variable.
void mem_swap(void* a, void* b, size_t n);

/// Swaps the given values using `t` as temporary variable.
void mem_swap_with(void* a, void* b, void* t, size_t n);

#endif // MY_MEM_H
