#ifndef MY_UTIL_H
#define MY_UTIL_H

#include "my_stuff/core.h"

#define TERM_RESET "\033[0m"
#define TERM_BLACK(str) "\x1B[30m" str TERM_RESET
#define TERM_RED(str) "\x1B[31m" str TERM_RESET
#define TERM_GREEN(str) "\x1B[32m" str TERM_RESET
#define TERM_YELLOW(str) "\x1B[33m" str TERM_RESET
#define TERM_BLUE(str) "\x1B[34m" str TERM_RESET
#define TERM_MAGENTA(str) "\x1B[35m" str TERM_RESET
#define TERM_CYAN(str) "\x1B[36m" str TERM_RESET
#define TERM_LIGHT_GREY(str) "\x1B[37m" str TERM_RESET
#define TERM_DARK_GREY(str) "\x1B[90m" str TERM_RESET
#define TERM_LIGHT_RED(str) "\x1B[91m" str TERM_RESET
#define TERM_LIGHT_GREEN(str) "\x1B[92m" str TERM_RESET
#define TERM_LIGHT_YELLOW(str) "\x1B[93m" str TERM_RESET
#define TERM_LIGHT_BLUE(str) "\x1B[94m" str TERM_RESET
#define TERM_LIGHT_MAGENTA(str) "\x1B[95m" str TERM_RESET
#define TERM_LIGHT_CYAN(str) "\x1B[96m" str TERM_RESET
#define TERM_WHITE(str) "\x1B[97m" str TERM_RESET

/// Replaces the first '\n' found by '\0'.
void cut_at_newline(char* s);

/// Returns a new file path with the extension `ext`.
char* change_extension(const char* path, const char* ext);

/// Prints a message and waits for an input.
/// Loops while the format is not respected.
void ask(const char* msg, const char* format, void* ptr);

/// Prints a message and waits for a string input.
void ask_str(const char* msg, char* s, size_t max_len);

/// Clones the given string, allocating the necessary memory.
char* str_clone(const char* s);

/// Surrounds the given string with the given prefix and suffix.
char* str_surround(const char* prefix, const char* s, const char* suffix);

#endif // MY_UTIL_H
