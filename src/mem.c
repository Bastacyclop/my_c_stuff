#include "my_stuff/mem.h"
#include "my_stuff/log.h"

#include <alloca.h>
#include <string.h>

static
void assert_alloc(void* mem);

void* mem_alloc(size_t n) {
    void* mem = malloc(n);
    assert_alloc(mem);
    return mem;
}

void assert_alloc(void* mem) {
    if (!mem) fatal("allocation error");
}

void mem_free(void* mem) {
    free(mem);
}

void* mem_alloc_zeroed(size_t n) {
    void* mem = calloc(1, n);
    assert_alloc(mem);
    return mem;
}

void* mem_alloc_aligned(size_t n, size_t align) {
    void* mem = aligned_alloc(align, n);
    assert_alloc(mem);
    return mem;
}

void* mem_realloc(void* mem, size_t n) {
    mem = realloc(mem, n);
    assert_alloc(mem);
    return mem;
}

void mem_set(void* mem, uint8_t byte, size_t n) {
    memset(mem, byte, n);
}

void mem_copy(const void* src, void* dst, size_t n) {
    memcpy(dst, src, n);
}

void mem_swap(void* a, void* b, size_t n) {
    void* t = alloca(n);
    mem_swap_with(a, b, t, n);
}

void mem_swap_with(void* a, void* b, void* t, size_t n) {
    mem_copy(a, t, n);
    mem_copy(b, a, n);
    mem_copy(t, b, n);
}
