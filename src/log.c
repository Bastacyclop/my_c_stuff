#include "my_stuff/log.h"

static FILE* log_file = NULL;

FILE* get_log_file() {
    if (!log_file) {
        log_file = stdout;
    }

    return log_file;
}

void set_log_file(FILE* f) {
    log_file = f;
}
