#include "my_stuff/util.h"
#include "my_stuff/mem.h"

#include <string.h>

void cut_at_newline(char* s) {
    char* pos = strchr(s, '\n');
    if (pos) { *pos = '\0'; }
}

char* change_extension(const char* path, const char* ext) {
    char* sep = strchr(path, '.');
    assert(sep);
    size_t name_len = sep - path;
    size_t ext_len = strlen(ext);

    // +1 for `.` and +1 for `\0`
    char* res = mem_alloc((name_len + 1 + ext_len + 1)*sizeof(char));
    mem_copy(path, res, name_len);
    res[name_len] = '.';
    mem_copy(ext, res + name_len + 1, ext_len + 1);
    return res;
}

void ask(const char* msg, const char* format, void* ptr) {
    char input[255];
    while (true) {
        ask_str(msg, input, 255);
        if (sscanf(input, format, ptr) == 1) break;
    }
}

void ask_str(const char* msg, char* s, size_t max_len) {
    fputs(msg, stdout);
    fgets(s, max_len, stdin);
    cut_at_newline(s);
}

char* str_clone(const char* s) {
    size_t len = strlen(s);
    char* clone = mem_alloc((len + 1)*sizeof(char)); // + 1 for '\0'
    mem_copy(s, clone, (len + 1));
    return clone;
}

char* str_surround(const char* prefix, const char* s, const char* suffix) {
    size_t pre_len = strlen(prefix);
    size_t s_len = strlen(s);
    size_t suf_len = strlen(suffix);

    char* res = mem_alloc(pre_len + s_len + suf_len + 1); // + 1 for '\0'
    mem_copy(prefix, res, pre_len);
    mem_copy(s, res + pre_len, s_len);
    mem_copy(suffix, res + pre_len + s_len, suf_len + 1);

    return res;
}
