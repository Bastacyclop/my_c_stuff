#include "my_stuff/core.h"

void crash() {
    exit(1);
}

bool is_power_of_two(size_t n) {
    return ((n & (n - 1)) == 0) && (n != 0);
}

size_t next_power_of_two(size_t n) {
    n--;
    n |= (n >> 1);  //  2 bits
    n |= (n >> 2);  //  4 bits
    n |= (n >> 4);  //  8 bits
    n |= (n >> 8);  // 16 bits
    n |= (n >> 16); // 32 bits
    n |= (n >> 32); // 64 bits
    n++;
    return n;
}

Option(size_t) checked_next_power_of_two(size_t n) {
    size_t mb = next_power_of_two(n);
    if (mb < n) return (Option(size_t)) None;
    return (Option(size_t)) Some(mb);
}

#define impl_max_min_for(T)         \
    T T##_max(T a, T b) {           \
        return (a > b) ? (a) : (b); \
    }                               \
                                    \
    T T##_min(T a, T b) {           \
        return (a < b) ? (a) : (b); \
    }

impl_max_min_for(size_t)
impl_max_min_for(uint8_t)
impl_max_min_for(uint16_t)
impl_max_min_for(uint32_t)
impl_max_min_for(uint64_t)
impl_max_min_for(int8_t)
impl_max_min_for(int16_t)
impl_max_min_for(int32_t)
impl_max_min_for(int64_t)
impl_max_min_for(float)
impl_max_min_for(double)
